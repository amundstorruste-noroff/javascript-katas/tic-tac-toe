import "./App.css";
import Board from "./components/Board";
import React, { useState } from "react";
import calculateWinner from "./util/calculateWinner";

function App() {
  const [board, setBoard] = useState(Array(9).fill(null));
  const [xIsNext, setXisNext] = useState(true);
  const winner = calculateWinner(board);

  const onClick = (i) => {
    const boardCopy = [...board];
    // If user click an occupied square or if game is won, return
    if (winner || boardCopy[i]) return;
    // Put an X or an O in the clicked square
    boardCopy[i] = xIsNext ? "X" : "O";
    setBoard(boardCopy);
    setXisNext(!xIsNext);
  };

  return (
    <div className="App">
      <div className="grid">
        <Board squares={board} onClick={onClick}></Board>
      </div>
      <p>{winner ? "Winner: " + winner : "Next Player: " + (xIsNext ? "X" : "O")}</p>
    </div>
  );
}

export default App;
